import os

import MySQLdb

from flask import Flask, request, render_template


app = Flask(__name__)


DEFAULT_APACHE_SITE_CONFIG = '/etc/apache2/sites-enabled/{id}'
DEFAULT_APACHE_ROOT_DIRECTORY = '/var/www/{id}/www'

APACHE_SITE_CONFIG = os.environ.get('PHM_APACHE_SITE_CONFIG', DEFAULT_APACHE_SITE_CONFIG)
APACHE_ROOT_DIRECTORY = os.environ.get('PHM_APACHE_ROOT_DIRECTORY', DEFAULT_APACHE_ROOT_DIRECTORY)

DEFAULT_MYSQL_USER = 'root'
DEFAULT_MYSQL_PASSWD = 'root'
DEFAULT_MYSQL_HOST = '127.0.0.1'
DEFAULT_MYSQL_PORT = '3306'

MYSQL_USER = os.environ.get('PHM_MYSQL_USER', DEFAULT_MYSQL_USER)
MYSQL_PASSWD = os.environ.get('PHM_MYSQL_PASSWD', DEFAULT_MYSQL_PASSWD)
MYSQL_HOST = os.environ.get('PHM_MYSQL_HOST', DEFAULT_MYSQL_HOST)
MYSQL_PORT = int(os.environ.get('PHM_MYSQL_PORT', DEFAULT_MYSQL_PORT))

DEFAULT_FTP_ROOT_DIRECTORY = '/var/www/{id}'
FTP_ROOT_DIRECTORY = os.environ.get('PHM_FTP_ROOT_DIRECTORY', DEFAULT_FTP_ROOT_DIRECTORY)


OK = 0
ERROR = 1
ERROR_BAD_REQUEST = 2
ERROR_APACHE_SITE_IS_EXISTS = 3
ERROR_WRITE_FILE = 4
ERROR_MYSQL = 5


def is_exists(path):
    return os.path.exists(path)


def system(cmd):
    return os.system(cmd)


def write_file(filename, template, params={}):
    try:
        f = open(filename, 'w')
        f.write(render_template(template, **params))
        f.close()
        return 0
    except IOError:
        return ERROR_WRITE_FILE


def apache_id_from_server_name(server_name):
    return server_name.replace('.', '__').replace('-', '__')


def mysql_sql(sql):
    con = None
    try:
        con = MySQLdb.connect(host=MYSQL_HOST,
                              user=MYSQL_USER,
                              passwd=MYSQL_PASSWD,
                              port=MYSQL_PORT)
        cur = con.cursor()
        cur.execute(sql)
        return 0, cur.fetchall()
    except MySQLdb.Error, e:
        print "Error %d: %s" % (e.args[0], e.args[1])
        return ERROR_MYSQL, None
    finally:
        if con:
            con.close()


def create_system_user(username, home):
    return system('useradd -d %s %s' % (home, username))


def chown(username, path):
    return system('chown %s %s -R' % (username, path))


def set_system_user_password(username, passwd):
    return system("echo '%s\n%s' | passwd %s" % (passwd, passwd, username))


@app.route('/hosting/create/')
def apache_create():
    id = request.args.get('id')
    server_name = request.args.get('server_name')
    server_aliases = request.args.get('server_aliases')

    if not id or not server_name:
        return 'ERR %s' % ERROR_BAD_REQUEST

    user = 'user%s' % id

    apache_conf = APACHE_SITE_CONFIG.format(id=user)
    user_root = FTP_ROOT_DIRECTORY.format(id=user)
    document_root = APACHE_ROOT_DIRECTORY.format(id=user)

    if is_exists(apache_conf):
        return 'ERR %s' % ERROR_APACHE_SITE_IS_EXISTS

    res = write_file(apache_conf, 'apache/site', {
        'server_name': server_name,
        'server_aliases': server_aliases,
        'document_root': document_root,
        'user': user,
    })
    if res:
        return 'ERR %s' % res

    os.makedirs(document_root)
    s = create_system_user(user, user_root)
    if s:
        return 'ERR %s' % ERROR
    s = chown(user, user_root)
    if s:
        return 'ERR %s' % ERROR
    system('service apache2 restart')
    return 'OK 0'


@app.route('/ftp/setpassword/')
def ftp_setpassword():
    username = request.args.get('username')
    passwd = request.args.get('passwd')

    if not username or not passwd:
        return 'ERR %s' % ERROR_BAD_REQUEST

    s = set_system_user_password(username, passwd)
    if s:
        return 'ERR %s' % ERROR
    return 'OK 0'


@app.route('/mysql/user/create/')
def mysql_user_create():
    user = request.args.get('user')
    passwd = request.args.get('passwd')

    if not user or not passwd:
        return 'ERR %s' % ERROR_BAD_REQUEST

    status, results = mysql_sql("CREATE USER '{user}'@'%' IDENTIFIED BY '{passwd}';".format(user=user, passwd=passwd))
    if status:
        return 'ERR %s' % status
    status, results = mysql_sql("GRANT ALL PRIVILEGES ON `{user}\_%` . * TO '{user}'@'%';".format(user=user))
    if status:
        return 'ERR %s' % status
    return 'OK 0'


@app.route('/mysql/user/setpassword/')
def mysql_user_setpassword():
    user = request.args.get('user')
    passwd = request.args.get('passwd')

    if not user or not passwd:
        return 'ERR %s' % ERROR_BAD_REQUEST

    status, results = mysql_sql("SET PASSWORD FOR '{user}'@'%' = PASSWORD('{passwd}');".format(user=user, passwd=passwd))
    if status:
        return 'ERR %s' % status
    return 'OK 0'


@app.route('/mysql/db/create/')
def mysql_db_create():
    name = request.args.get('name')

    if not name:
        return 'ERR %s' % ERROR_BAD_REQUEST

    status, results = mysql_sql("CREATE DATABASE `{name}` CHARACTER SET utf8 COLLATE utf8_general_ci;".format(name=name))
    if status:
        return 'ERR %s' % status
    return 'OK 0'


if __name__ == '__main__':
    app.debug = True
    app.run('0.0.0.0')
